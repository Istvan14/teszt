﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cégnyilvántartás
{
    class Pair<T>
    {
        public T Item1 { get; }
        public T Item2 { get; }

        public Pair(T item1, T item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        public delegate void Esemény(Pair<T> item);

        public event Esemény EgyesItem;

        public event Esemény KettesItem;
    }
}
