﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cégnyilvántartás
{
    public  class ForProfit : Cég
    {
        public ForProfit(string name) : base(name)
        {

        }

        public override void WriteDescription()
        {
            Console.WriteLine("Pénzt termelek a tulajdonosnak.");
        }

        public void BuyStock()
        {
            Console.WriteLine("Részvényt veszek ki.");

        }

    }
}
