﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Cégnyilvántartás
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Cég> list = new List<Cég>();

            ForProfit Cég1 = new ForProfit("Cég1");
            Cég1.Address = "Budapest";
            NonProfit Cég2 = new NonProfit("Cég2");
            Cég2.Address = "Pécs";
            Bróker Cég3 = new Bróker("Cég3");
            Cég3.Address = "Szeged";

            list.Add(Cég1);
            list.Add(Cég2);
            list.Add(Cég3);

            foreach (Cég item in list)
            {
                item.WriteData();
                item.WriteDescription();
                Console.WriteLine("");

            }


            List<ForProfit> list2 = new List<ForProfit>();

            list2.Add(Cég1);
            list2.Add(Cég3);

            foreach (ForProfit item in list2)
            {
                item.BuyStock();
                Console.WriteLine("");

            }


            Pair<string> pair = new Pair<string>("valami1", "valami2");
            Console.WriteLine(pair.Item1 + ", " + pair.Item2);


            string path = Console.ReadLine();
            bool IsFileExist = File.Exists(path);
            if (IsFileExist == false)
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Létezett");
                }
            }
            else
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }


            Console.ReadKey();
        }


        //Extension(List<T>)
        //{
        //    return List[1];
        //}
    }
}
