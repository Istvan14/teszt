﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cégnyilvántartás
{
    public abstract class Cég
    {
        public string Name { get; }

        public string Address { get; set; }


        public Cég(string name)
        {
            Name = name;
        }

        public Cég(string name, string address) : this(name)
        {
            Address = address;
        }

        public void WriteData()
        {
            Console.WriteLine(Name +", " + Address);
        }

        public abstract void WriteDescription();
    }
}
